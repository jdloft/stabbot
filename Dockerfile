FROM ubuntu:19.04

###########################
### from ubuntu ###########
###########################

LABEL cache_breaker=2020_01_01

### general
# Not sure if all of these are needed...
#RUN apt-get update && apt-get install -y git tar curl nano wget dialog net-tools build-essential
RUN apt-get update && apt-get install -y git tar wget build-essential

### building ffmpeg itself
RUN apt-get update && apt-get -y install \
    autoconf automake build-essential libtool pkg-config texinfo wget nasm \
    libass-dev libfreetype6-dev libsdl2-dev libtheora-dev \
    libva-dev libvdpau-dev libvorbis-dev libxcb1-dev \
    libxcb-shm0-dev libxcb-xfixes0-dev zlib1g-dev

### building ffmpeg-libs
RUN apt-get update && apt-get -y install yasm cmake mercurial cmake-curses-gui \
    libx264-dev libx265-dev libnuma-dev libfdk-aac-dev libmp3lame-dev libopus-dev libvpx-dev

### python
RUN apt-get update && apt-get install -y python3-pip libffi-dev libssl-dev python3-dev


###########################
### ffmpeg ################
###########################

RUN mkdir ~/ffmpeg_sources && mkdir ~/ffmpeg_build && mkdir ~/bin

# path where ffmpeg-binaries are emmited to and read from
ENV PATH="~/bin:${PATH}"

# path where ffmpeg looks for vid.stab
# note: $HOME is not available for docker-commands, only for bash-commands. https://github.com/moby/moby/issues/28971
ENV LD_LIBRARY_PATH /root/ffmpeg_build/lib:$LD_LIBRARY_PATH

# vid.stab
RUN cd ~/ffmpeg_sources \
  && git clone https://github.com/georgmartius/vid.stab.git \
  && cd vid.stab \
  && cmake -DCMAKE_INSTALL_PREFIX:PATH=$HOME/ffmpeg_build . \
  && make \
  && make install


## ffmpeg
RUN cd ~/ffmpeg_sources \
    && wget http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 \
    && tar xjvf ffmpeg-snapshot.tar.bz2 \
    && cd ffmpeg \
    && PATH="$HOME/bin:$PATH"   PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
      --prefix="$HOME/ffmpeg_build" \
      --pkg-config-flags="--static" \
      --extra-cflags="-I$HOME/ffmpeg_build/include" \
      --extra-ldflags="-L$HOME/ffmpeg_build/lib" \
      --bindir="$HOME/bin" \
      --enable-gpl \
      --enable-libass \
      --enable-libfdk-aac \
      --enable-libfreetype \
      --enable-libmp3lame \
      --enable-libopus \
      --enable-libtheora \
      --enable-libvorbis \
      --enable-libvpx \
      --enable-libx264 \
      --enable-libx265 \
      --enable-nonfree \
      --enable-libvidstab \
    && PATH="$HOME/bin:$PATH" make \
    && make install \
    && hash -r

# workaround: ffprobe-wrapper doesn't find ffprobe otherwise
RUN cp -r ~/bin/* /bin


###########################
### prismedia #############
###########################

RUN git clone https://git.lecygnenoir.info/LecygneNoir/prismedia.git /prismedia
RUN cd /prismedia && git checkout $(git describe --tags $(git rev-list --tags --max-count=1))

###########################
### python ################
###########################


WORKDIR /bot
COPY requirements.txt /bot
RUN pip3 install -r requirements.txt

COPY src/ /bot

ENV PYTHONUNBUFFERED 0
CMD ["python3", "bot.py"]

# BUILD_DATE is set in .gitlab-ci.yml
# this is a cache-breaker to make sure the following commands
# don't get cached for too long
ARG BUILD_DATE=unknown

# display the arg, so it shows up in the log in gitlab CI
RUN echo $BUILD_DATE

# youtube-dl is volatile and requires fast updates
RUN pip3 install -U youtube-dl
